import psycopg2, sys
import pandas as pd
import numpy as np
from time import time
from tqdm import tqdm
from copy import deepcopy
sys.path.insert(1,"./utils")
from utilities import *

query_trimbow_prescr = "WITH tot as (SELECT patient, \
                        prescrdate, \
                        prescrdate as init_therapy, \
                        prescrdate + quantity * interval '30' day as end_therapy, \
                        pack, \
                        barcode, \
                        '120' as dosage, \
                        quantity, \
                        'in' as stage, \
                        doctor, \
                        d.tag, \
                        icd10 \
                    FROM (SELECT DISTINCT patient, prescrdate, pack, barcode, doctor, quantity, icd10 \
                        FROM full_wh \
                        WHERE barcode in ('2803172601024')) fwh LEFT JOIN doctor_predicted_tag d ON(d.doctor_hash = fwh.doctor) \
                    WHERE patient IS NOT NULL \
                    ORDER BY patient, prescrdate, dosage) \
                    SELECT *, LEAD (prescrdate) OVER (PARTITION BY patient ORDER BY prescrdate) AS \"next\" \
                    FROM tot"

query_patients = "SELECT DISTINCT patient FROM public.full_wh WHERE barcode IN ('2803172601024') ORDER BY patient"

query_full_other = "SELECT DISTINCT patient, prescrdate as days, barcode, pack \
              FROM public.full_wh \
              WHERE barcode NOT IN ('2803172601024')"
query_full_trimbow = "SELECT DISTINCT patient, prescrdate, barcode, pack \
              FROM public.full_wh \
              WHERE barcode IN ('2803172601024')"

def expand_bounds(df,s,closeness):
    temp = deepcopy(df)
    temp["days"] = temp.apply(lambda x: pd.date_range(x["init"],x["end"],freq="D",closed=closeness),axis=1)
    temp["stage"] = s
    temp = temp[["patient","days","stage"]]
    return temp.explode("days")

def expand_trimbow(df,cols):
    temp = deepcopy(df)
    temp["days"] = temp.apply(lambda x: pd.date_range(x["init_therapy"],x["end_therapy"],freq="D",closed="left"),axis=1)
    temp = temp[cols]
    return temp.explode("days")

def enumerate_presc(df,col):
    temp = deepcopy(df)
    enum = temp[col].values
    for i in tqdm(range(enum.size)):
        if enum[i] == 0:
            enum[i] = enum[i-1] + 1
    temp[[col]] = enum.astype(int)
    return temp

def adjust_dates(df):
    temp = deepcopy(df)
    temp["overlap"] = (temp["next"]-temp["end_therapy"]).apply(lambda x: abs(x.days) if x.days < 0 else 0)
    while temp["overlap"].sum() > 0:
        delay = pd.Series(temp["overlap"].values)
        rolled_delay = pd.Series(np.roll(temp["overlap"].values,1))
        temp[["init_therapy"]] = temp["init_therapy"] + pd.to_timedelta(rolled_delay,unit="d")
        temp[["end_therapy"]] = temp["end_therapy"] + pd.to_timedelta(rolled_delay,unit="d")
        temp[["next"]] = temp["next"] + pd.to_timedelta(delay,unit="d")
        temp[["overlap"]] = (temp["next"]-temp["end_therapy"]).apply(lambda x: abs(x.days) if x.days < 0 else 0)
    return temp.drop(["overlap"],axis=1)

def add_gaps_and_breaks(df,threshold):
    temp = deepcopy(df)
    temp["delay"] = (temp["next"]-temp["end_therapy"]).apply(lambda x: abs(x.days) if x.days > 0 else 0)
    for row in tqdm(temp.iterrows(),total=temp.shape[0]):
        if row[1]["delay"] > threshold:
            break_row = pd.DataFrame(columns=row[1].index.values)
            break_row.loc[0,"patient"] = row[1]["patient"]
            break_row.loc[0,"init_therapy"] = row[1]["end_therapy"]
            break_row.loc[0,"end_therapy"] = row[1]["next"]
            break_row.loc[0,"first"] = False
            break_row.loc[0,"stage"] = "break"
            break_row.loc[0,"prescrnum"] = 0
            temp = pd.concat([temp,break_row],axis=0)
        elif 0 < row[1]["delay"] <= threshold:
            gap_row = pd.DataFrame(columns=row[1].index.values)
            gap_row.loc[0,"patient"] = row[1]["patient"]
            gap_row.loc[0,"init_therapy"] = row[1]["end_therapy"]
            gap_row.loc[0,"end_therapy"] = row[1]["next"]
            gap_row.loc[0,"first"] = False
            gap_row.loc[0,"stage"] = "gap"
            gap_row.loc[0,"prescrnum"] = 0
            temp = pd.concat([temp,gap_row],axis=0)
        else:
            continue
    return temp.sort_values(["patient","init_therapy"])

def label_hiatus(df,hiatus):
    temp = deepcopy(df)
    init_cols = temp.columns
    temp[hiatus+"num"] = temp["stage"].apply(lambda x: 0 if x == hiatus else np.nan)
    temp_hiatus = temp[temp[hiatus+"num"]==0]
    temp_hiatus_first = temp_hiatus.groupby(["patient"]).first().reset_index()[temp.columns]
    temp_hiatus_first[[hiatus+"num"]] = 1
    init_therapy = temp_hiatus_first["init_therapy"].astype('O')
    end_therapy = temp_hiatus_first["end_therapy"].astype('O')
    temp_hiatus_first.drop(["init_therapy","end_therapy"],axis=1,inplace=True)
    temp_hiatus_first["init_therapy"] = init_therapy
    temp_hiatus_first["end_therapy"] = end_therapy
    temp_hiatus = pd.merge(temp_hiatus[init_cols],temp_hiatus_first[["patient","init_therapy","end_therapy",hiatus+"num"]],on=["patient","init_therapy","end_therapy"],how="left")
    temp_hiatus[[hiatus+"num"]] = temp_hiatus[hiatus+"num"].fillna(0)
    temp_hiatus = enumerate_presc(temp_hiatus,hiatus+"num")
    temp = pd.merge(temp[init_cols.values.tolist()],temp_hiatus,on=init_cols.values.tolist(),how="left")
    temp[[hiatus+"num"]] = temp[[hiatus+"num"]].fillna(0).astype('int')
    return temp

def label_doctor(df,patientset):
    temp = deepcopy(df)
    for p in patientset.values.reshape(1,-1).tolist()[0]:
        orderlist = temp.loc[temp["patient"]==p,"doctor"].unique().tolist()
        values = temp.loc[temp["patient"]==p,"doctor"].values.tolist()
        temp.loc[temp["patient"]==p,"docnum"] = np.asarray([orderlist.index(a) for a in values]) + 1
    temp[["docnum"]] = temp[["docnum"]].astype("int")
    return temp

if __name__ == "__main__":
    config = read_config(relative_path="./")

    prod_conn, prod_cur = connect_postgres(config)

    # GET PATIENTS LIST
    nowprint("GET PATIENTS LIST")
    prod_cur.execute(query_patients)
    patients = fetch(prod_cur,-1)

    # GET FULL_WH WITH TRIMBOW
    nowprint("GET FULL_WH WITH TRIMBOW")
    prod_cur.execute(query_full_trimbow)
    full_wh_trimbow = fetch(prod_cur,-1)
    full_wh_trimbow[["prescrdate"]] = full_wh_trimbow["prescrdate"].dt.date

    # GET FULL_WH EXCEPT TRIMBOW
    nowprint("GET FULL_WH EXCEPT TRIMBOW")
    prod_cur.execute(query_full_other)
    full_wh_other = fetch(prod_cur,-1)
    full_wh_other[["days"]] = full_wh_other["days"].dt.date

    # GET TRIMBOW PRESCRIPTIONS
    nowprint("GET TRIMBOW PRESCRIPTIONS")
    prod_cur.execute(query_trimbow_prescr)
    trimbow_prescr = fetch(prod_cur,-1)
    trimbow_prescr.sort_values(["patient","prescrdate","dosage"],inplace=True) 

    # ELABORATE FIRST TRIMBOW PRESCRIPTIONS
    nowprint("ELABORATE FIRST TRIMBOW PRESCRIPTIONS")
    trimbow_first_presc = trimbow_prescr.groupby("patient").first().reset_index()[["patient","prescrdate","init_therapy","dosage","quantity","pack","barcode","doctor"]]
    trimbow_first_presc["first"] = True
    trimbow_first_presc["prescrnum"] = 1

    # ADD LABELS, ADJUST THERAPY DATES, ADD GAPS BETWEEN TRIMBOW THERAPIES
    nowprint("ADD LABELS, ADJUST THERAPY DATES, ADD GAPS BETWEEN TRIMBOW THERAPIES")
    trimbow_prescr = pd.merge(trimbow_prescr,trimbow_first_presc,how="left",on=["patient","prescrdate","init_therapy","dosage","quantity","pack","barcode","doctor"])
    trimbow_prescr[["first"]] = trimbow_prescr[["first"]].fillna(False)
    trimbow_prescr[["prescrnum"]] = trimbow_prescr["prescrnum"].fillna(0)
    trimbow_prescr = enumerate_presc(trimbow_prescr,"prescrnum")
    trimbow_prescr = adjust_dates(trimbow_prescr)
    trimbow_prescr = label_doctor(trimbow_prescr,patients)
    trimbow_prescr_gapped = add_gaps_and_breaks(trimbow_prescr,7)
    trimbow_prescr_gapped.reset_index(drop=True,inplace=True)

    # ADD LABELS TO BREAKS
    nowprint("ADD LABELS TO BREAKS")
    trimbow_prescr_gapped = label_hiatus(trimbow_prescr_gapped,"break")
    trimbow_prescr_gapped = label_hiatus(trimbow_prescr_gapped,"gap")
    
    # EXPAND TRIMBOW AND GAP DAYS
    nowprint("EXPAND TRIMBOW AND GAP DAYS")
    trimbow = expand_trimbow(trimbow_prescr_gapped,["patient","days","prescrdate","prescrnum","breaknum","gapnum","docnum","first","dosage","quantity","stage","doctor",'tag',"icd10","barcode"])

    # COMPUTE BOUDARIES (PRE AND POST 6 MONTHS)
    nowprint("COMPUTE BOUDARIES (PRE AND POST 6 MONTHS)")
    boundaries = trimbow.groupby("patient").agg({"days":["min","max"]}).reset_index()
    boundaries.columns = [' '.join([u,d]).strip() for u,d in zip(boundaries.columns.get_level_values(0),boundaries.columns.get_level_values(1))]
    boundaries_pre = pd.DataFrame({"patient": boundaries["patient"].values,
                                   "init": boundaries["days min"] - pd.to_timedelta(14,unit="M"),
                                   "end": boundaries["days min"]})
    boundaries_post = pd.DataFrame({"patient": boundaries["patient"].values,
                                   "init": boundaries["days max"],
                                   "end": boundaries["days max"] + pd.to_timedelta(6,unit="M")})
    boundaries_pre = expand_bounds(boundaries_pre,"pre","left")
    boundaries_post = expand_bounds(boundaries_post,"post","right")                                   

    # ASSEMBLE THE PATIENT JOURNEY
    nowprint("ASSEMBLE THE PATIENT JOURNEY")
    patient_journey = pd.DataFrame(columns=trimbow.columns)
    for p in tqdm(patients["patient"]):
        p_pre = boundaries_pre[boundaries_pre["patient"] == p]
        p_trimbow = trimbow[trimbow["patient"] == p]
        p_post = boundaries_post[boundaries_post["patient"] == p]
        patient_journey = pd.concat([patient_journey,p_pre,p_trimbow,p_post],axis=0)
    patient_journey = patient_journey[trimbow.columns].reset_index(drop=True)
    # FINE CORRECTIONS
    patient_journey[["days"]] = patient_journey["days"].dt.date
    patient_journey.loc[:,"prescrdate"] = patient_journey["prescrdate"].apply(lambda x: np.nan if x is np.nan else x.to_pydatetime().date())
    patient_journey[["prescrnum"]] = patient_journey[["prescrnum"]].fillna(0)
    patient_journey[["breaknum"]] = patient_journey[["breaknum"]].fillna(0).astype("int")
    patient_journey[["gapnum"]] = patient_journey[["gapnum"]].fillna(0).astype("int")
    patient_journey[["docnum"]] = patient_journey[["docnum"]].fillna(0).astype("int")

    # ADD COMEDICATIONS
    nowprint("ADD COMEDICATIONS")
    medications_trimbow = pd.merge(patient_journey,full_wh_trimbow,on=["patient","prescrdate","barcode"],how="inner")
    medications_trimbow = medications_trimbow[["prescrdate","patient","barcode","prescrnum","breaknum","gapnum"]].drop_duplicates()
    medications_other = pd.merge(patient_journey.drop("barcode",axis=1),full_wh_other,on=["patient","days"],how="inner")
    medications_other = medications_other[["days","patient","barcode","prescrnum","breaknum","gapnum"]].drop_duplicates()
    
    # TRANSFERRING to DB
    nowprint("TRANSFERRING TO DB")
    sql = '''TRUNCATE public.reconstructed_trimbow_journey;'''
    prod_cur.execute(sql)
    sql = '''TRUNCATE public.medications;'''
    prod_cur.execute(sql)
        
    WriteFromStringIO(medications_trimbow,"public.medications",prod_conn)
    WriteFromStringIO(medications_other,"public.medications",prod_conn)
    pj_tbt = patient_journey[["days","patient","stage","first","dosage","doctor","tag","icd10","prescrnum","breaknum","gapnum","docnum"]]
    pj_tbt[["first"]] = pj_tbt[["first"]].fillna(False)
    pj_tbt = pj_tbt.where((pd.notnull(pj_tbt)),None)
    WriteFromStringIO(pj_tbt,"public.reconstructed_trimbow_journey",prod_conn)

    close_conns(prod_cur,prod_conn)


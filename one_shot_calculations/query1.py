import psycopg2, sys
import pandas as pd
import numpy as np
from time import time
from tqdm import tqdm
from copy import deepcopy
sys.path.insert(1,"../utils")
from utilities import *

# query = "WITH cats as (SELECT patient_hash, ARRAY_TO_STRING(ARRAY_AGG(category_chiesi),', ') \
# 				FROM reconstructed_trimbow_journey rtj JOIN medications med USING(caldate,patient_hash) \
# 														JOIN trimbow_druglist_final tdu USING(barcode) \
# 				WHERE rtj.stage = 'pre' \
# 				GROUP BY patient_hash) \
# SELECT tp.patient, \"c\".array_to_string, tp.first_prescription, tp.first_trimbow FROM cats \"c\" RIGHT JOIN trimbow_patients tp ON(\"c\".patient_hash = tp.patient)"
query = "WITH cat_pat_base as (SELECT tp.patient, CASE WHEN category_chiesi ='ICS NEBULES' THEN 'ICS' ELSE category_chiesi END as category_chiesi, \
					  med.caldate \
				FROM reconstructed_trimbow_journey rtj JOIN medications med USING(caldate,patient_hash) \
														JOIN trimbow_druglist_final tdu USING(barcode) \
														JOIN trimbow_patients tp ON(rtj.patient_hash = tp.patient) \
				WHERE rtj.stage = 'pre' AND tp.pre_eligible \
			                            AND switching_analysis=true \
			                            AND tp.patient_label <> 'excluded' \
			                         	AND category_chiesi NOT IN ('SABA/SAMA', 'SABA','SAMA') \
				                        AND rtj.caldate BETWEEN first_trimbow - interval '180 day' AND  first_trimbow \
		   ), cat_pat_max as (SELECT patient, max(caldate) as caldate FROM cat_pat_base GROUP BY patient), \
			finale as (SELECT tp.patient, tp.first_prescription, tp.first_trimbow, cpb.category_chiesi, cpb.caldate FROM cat_pat_base cpb JOIN cat_pat_max USING(patient, caldate) \
									   RIGHT JOIN trimbow_patients tp USING(patient)) \
SELECT patient, CASE WHEN array_to_string(ARRAY_AGG(DISTINCT category_chiesi),', ') = '' THEN NULL ELSE array_to_string(ARRAY_AGG(DISTINCT category_chiesi),', ') END, first_prescription, first_trimbow \
FROM finale \
GROUP BY patient, first_prescription, first_trimbow"

if __name__ == "__main__":
    config = read_config(relative_path="../")

    prod_conn, prod_cur = connect_postgres(config)

    prod_cur.execute(query)
    patients = fetch(prod_cur,-1) 

    patients['treatments'] = ''
    patients['treatments_category_num'] = 0

    patients["ICS"] = patients.array_to_string.str.contains('ICS')
    patients["ICS"] = patients["ICS"].fillna(False)
    patients["LABA"] = patients.array_to_string.str.contains('LABA')
    patients["LABA"] = patients["LABA"].fillna(False)
    patients["LAMA"] = patients.array_to_string.str.contains('LAMA')
    patients["LAMA"] = patients["LAMA"].fillna(False)

    patients.loc[patients['ICS'],'treatments'] = patients.loc[patients['ICS'],'treatments'] + 'ICS, '
    patients.loc[patients['ICS'],'treatments_category_num'] = patients.loc[patients['ICS'],'treatments_category_num'] + 1
    patients.loc[patients['LABA'],'treatments'] = patients.loc[patients['LABA'],'treatments'] + 'LABA, '
    patients.loc[patients['LABA'],'treatments_category_num'] = patients.loc[patients['LABA'],'treatments_category_num'] + 1
    patients.loc[patients['LAMA'],'treatments'] = patients.loc[patients['LAMA'],'treatments'] + 'LAMA'
    patients.loc[patients['LAMA'],'treatments_category_num'] = patients.loc[patients['LAMA'],'treatments_category_num'] + 1

    patients['treatments'] = patients['treatments'].apply(lambda x: x[:-2] if x[-2:] == ', ' else x)
    patients['treatments'] = patients['treatments'].replace("","NA")
    patients['treatments_num'] = patients['treatments'].apply(lambda x: len(x.split(', ')))
    patients.loc[patients['treatments'] == 'NA','treatments_num'] = 0

    counts = patients.groupby('treatments').count().reset_index()[['treatments','patient']]
    patients[['patient','treatments', 'treatments_num','first_prescription','first_trimbow']].to_csv('../outputs/patient_distr_treatment_cat_pre.csv',header=True,index=False,sep='|')
    counts.to_csv('../outputs/patient_distr_treatment_cat_pre_count.csv',header=True,index=False,sep='|')
import pandas as pd
import numpy as np
from hashlib import sha256
import psycopg2, sys, re, os, json, argparse, pymongo
from psycopg2.extras import DictCursor
from tqdm import tqdm
from io import StringIO
from copy import deepcopy
from time import time, strftime

def nowprint(msg):
    print("< " + strftime("%H:%M:%S") + " => " + msg)

def close_conns(cur,conn):
    nowprint("Closing connections...")
    cur.close()
    conn.close()
    nowprint("Connections closed")

def connect_postgres(config,curname=None):
    lab_connection = psycopg2.connect(host=config["postgres_host"],
                                    port=config["postgres_port"],
                                    user=config["postgres_user"],
                                    password=config["postgres_password"],
                                    dbname=config["postgres_db"])
    if curname:
        lab_cursor = lab_connection.cursor(name=curname,withhold=True)
    else:
        lab_cursor = lab_connection.cursor()
    nowprint("Connected to server")
    return lab_connection, lab_cursor

def fetch(cur,howmany=-1):
    rows = ()
    if howmany == -1:
        rows = cur.fetchall()
    else:
        rows = cur.fetchmany(howmany)
    columns = list(map(lambda x: x.name, cur.description))
    to_list = list(map(lambda x: list(x),rows))
    df = pd.DataFrame(to_list,columns=columns)
    return df

def WriteFromStringIO(df, table_name, connection):
    output = StringIO()
    df.to_csv(output, sep = '|', index = False, header = False)
    output.seek(0)
    cursor = connection.cursor(cursor_factory = DictCursor)
    cursor.copy_from(output, table_name, sep = '|', null='NaN')
    connection.commit()
    cursor.close()

def write_to_xlsx(filename,df):
    writer = pd.ExcelWriter(filename, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Raw_Data',index=False)
    writer.save()

def get_args():
	parser = argparse.ArgumentParser(description='ABSoNeS', add_help=True)

	parser.add_argument('-q', '--mongo_query', action='store', type=str, default='{}',
		help='specifies the network type.')
	ret = parser.parse_args()

	return ret

def connect_mongo(url,db_name,coll_name):
    client = pymongo.MongoClient(url)
    db = client[db_name]
    collection = db[coll_name]
    return client, db, collection

def read_config(relative_path=""):
    json_data = {}
    with open(relative_path+'config.json') as json_file:
        json_data = json.load(json_file)
    return json_data

def remove_from_l(arr,r):
    l = deepcopy(arr)
    for t in r:
        flag = False
        while not flag:
            try:
                l.remove(t)
            except ValueError:
                flag = True
    return l

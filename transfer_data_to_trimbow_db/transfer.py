import pandas as pd
import numpy as np
import psycopg2
import psycopg2.extras
import sqlalchemy, argparse, sys
from datetime import datetime
from io import StringIO
sys.path.insert(1,"./utils")
from utilities import *

RELATIVE_PATH = "./transfer_data_to_trimbow_db/"

def args():
    parser = argparse.ArgumentParser(description='trimbow', add_help=True)

    parser.add_argument('-t', '--truncate_fullwh', action='store_true', help='force full_wh truncation')
    parser.add_argument('-bs', '--query_batch_start', action='store', type=str, default='', help='force full_wh truncation')
    parser.add_argument('-be', '--query_batch_end', action='store', type=str, default='', help='force full_wh truncation')
    ret = parser.parse_args()

    return ret

def write_log(text):
    with open('log.txt', 'a+') as f:
        f.write(text)
    print(text)
    
def WriteFromStringIO(df, table_name, connection):
    output = StringIO()
    df.to_csv(output, sep = '|', index = False, header = False)
    output.seek(0)
    cursor = connection.cursor(cursor_factory = psycopg2.extras.DictCursor)
    cursor.copy_from(output, table_name, sep = '|', null = '')
    connection.commit()
    cursor.close()

if __name__ == '__main__':
    config = read_config(relative_path="./transfer_data_to_trimbow_db/")
    args = args()
    
    connection_string ='postgresql://'+config["src_user"]+':'+config["src_password"]+'@'+config["src_host"]+'/'+config["src_db"]
    engine = sqlalchemy.create_engine(connection_string)
    connection = psycopg2.connect(connection_string)    

    connection_string_output ='postgresql://'+config["dst_user"]+':'+config["dst_password"]+'@'+config["dst_host"]+':'+config["dst_port"]+'/'+config["dst_db"]
    engine_output = sqlalchemy.create_engine(connection_string_output)
    connection_output = psycopg2.connect(connection_string_output) 
    
    write_log('--------------\n')
    write_log(str(datetime.now()) + ': STARTING PROCEDURE\n')
    
    if args.truncate_fullwh:
        #%%
        write_log(str(datetime.now()) + ': Truncation of full_wh table STARTED\n')
        
        sql = '''TRUNCATE public.full_wh;'''
        cursor = connection_output.cursor(cursor_factory = psycopg2.extras.DictCursor)
        cursor.execute(sql)
        connection_output.commit()
        cursor.close()
        
        write_log(str(datetime.now()) + ': Truncation of full_wh table ENDED\n')
    
    #%%   
    
    write_log(str(datetime.now()) + ': Loading of data STARTED (batch {} to {})\n'.format(args.query_batch_start,args.query_batch_end))
    
    with open('./transfer_data_to_trimbow_db/query.sql', 'r') as f:
        sql = f.read()
        
    df_full_wh = pd.read_sql(sql.format(config["query_start"],config["query_end"],config["query_months"],args.query_batch_start,args.query_batch_end), engine)
    df_full_wh['icd10'] = df_full_wh['icd10'].astype(str).str.replace('[', '{').str.replace(']','}')

    write_log(str(datetime.now()) + ': Loading of data ENDED (batch {} to {})\n'.format(args.query_batch_start,args.query_batch_end))
    #%% write results 
    
    write_log(str(datetime.now()) + ': Exporting on full_wh STARTED - {} lines\n'.format(str(df_full_wh.shape[0])))
    
    WriteFromStringIO(df_full_wh, 'public.full_wh', connection_output)
    
    write_log(str(datetime.now()) + ': Exporting on full_wh ENDED\n')
    
    #%%
   
    write_log(str(datetime.now()) + ': END\n--------------\n')
    

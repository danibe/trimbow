import pandas as pd
import numpy as np
import psycopg2
import psycopg2.extras
import sqlalchemy, argparse, sys
from datetime import datetime
from io import StringIO
sys.path.insert(1,"./utils")
from utilities import *

query = "with ph_data as ( \
    SELECT \
    ph.code as pharmacy, \
    to_char(s.prescrdate, 'YYYY-MM') as year_month, \
    sum(case when length(pt.hash) = 64 then 1 else 0 end)::float / count(*)::float as ratio \
    FROM \
    public.sales s \
    JOIN public.patient pt USING(patientdimid) \
    JOIN public.pharmacy ph USING(pharmacydimid) \
    JOIN public.product pr USING(productdimid) \
    JOIN \
    ( \
        SELECT DISTINCT \
        pha.pharmacy as pharmacy, \
        aud.year_month \
        FROM \
        delivery_final.p_pha_panel pha \
        JOIN delivery_final.panel_audit_v2 aud USING (panel_seq) \
        WHERE aud.frequency = 'monthly' AND aud.audit = 'sales' \
    ) panel_phs ON (ph.code = panel_phs.pharmacy and to_char(s.prescrdate, 'YYYY-MM') = panel_phs.year_month) \
    WHERE s.prescrdate between \'{0}\' and \'{1}\' and pr.tos = 'P' and s.patientcontribution <> 100 \
    GROUP BY ph.code, to_char(s.prescrdate, 'YYYY-MM') \
) \
SELECT DISTINCT \
pharmacy \
FROM ph_data \
GROUP BY pharmacy \
HAVING sum(case when ratio > 0.60 then 1 else 0 end) >= {2}"

def args():
    parser = argparse.ArgumentParser(description='trimbow', add_help=True)

    parser.add_argument('-t', '--truncate_ph', action='store_true', help='force continuous pharmacy truncation')
    ret = parser.parse_args()

    return ret


if __name__ == '__main__':
    config = read_config(relative_path="./transfer_data_to_trimbow_db/")
    args = args()

    connection_string ='postgresql://'+config["src_user"]+':'+config["src_password"]+'@'+config["src_host"]+'/'+config["src_db"]
    engine = sqlalchemy.create_engine(connection_string)
    connection_src = psycopg2.connect(connection_string)    

    connection_string_dst ='postgresql://'+config["dst_user"]+':'+config["dst_password"]+'@'+config["dst_host"]+':'+config["dst_port"]+'/'+config["dst_db"]
    engine_dst = sqlalchemy.create_engine(connection_string_dst)
    connection_dst = psycopg2.connect(connection_string_dst) 

    if args.truncate_ph:
        sql = '''TRUNCATE public.panel_pharmacy;'''
        cursor = connection_dst.cursor(cursor_factory = psycopg2.extras.DictCursor)
        cursor.execute(sql)
        connection_dst.commit()
        cursor.close()

    phs = pd.read_sql(query.format(config["query_start"],config["query_end"],config["query_months"]), engine)

    WriteFromStringIO(phs, 'public.panel_pharmacy', connection_dst) 


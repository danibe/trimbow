import pandas as pd
import numpy as np
import psycopg2
import psycopg2.extras
import sqlalchemy, argparse, sys
from datetime import datetime
from io import StringIO
sys.path.insert(1,"./utils")
from utilities import *

query_docs1 = "SELECT d_1.hash, \
count(DISTINCT dd_1.tag) AS num_tag, \
ARRAY_AGG(dd_1.tag) \
FROM doctor d_1 \
FULL JOIN fbenetti.doctordictionary dd_1 ON d_1.doctorspeciality::text = dd_1.greek::text \
WHERE length(d_1.hash::text) = 64 AND dd_1.tag <> 'NULL' \
GROUP BY d_1.hash \
HAVING count(DISTINCT dd_1.tag) > 0"

query_docs2 = "SELECT DISTINCT doctor FROM full_wh WHERE doctor IS NOT NULL"

def args():
    parser = argparse.ArgumentParser(description='trimbow', add_help=True)

    parser.add_argument('-t', '--truncate_docs', action='store_true', help='force doctor truncation')
    ret = parser.parse_args()

    return ret


if __name__ == '__main__':
    config = read_config(relative_path="./transfer_data_to_trimbow_db/")
    args = args()

    connection_string ='postgresql://'+config["src_user"]+':'+config["src_password"]+'@'+config["src_host"]+'/'+config["src_db"]
    engine_src = sqlalchemy.create_engine(connection_string)
    connection_src = psycopg2.connect(connection_string)    

    connection_string_dst ='postgresql://'+config["dst_user"]+':'+config["dst_password"]+'@'+config["dst_host"]+':'+config["dst_port"]+'/'+config["dst_db"]
    engine_dst = sqlalchemy.create_engine(connection_string_dst)
    connection_dst = psycopg2.connect(connection_string_dst) 

    if args.truncate_docs:
        sql = '''TRUNCATE public.doc_spec;'''
        cursor = connection_dst.cursor(cursor_factory = psycopg2.extras.DictCursor)
        cursor.execute(sql)
        connection_dst.commit()
        cursor.close()
    
    docs_1 = pd.read_sql(query_docs1, engine_src)
    docs_1["array_agg"] = docs_1["array_agg"].apply(lambda x: str(x).replace("['","{").replace("']","}").replace("', '",", "))
    docs_2 = pd.read_sql(query_docs2, engine_dst)

    WriteFromStringIO(docs_1[docs_1.hash.isin(docs_2.doctor.values)], 'public.doc_spec', connection_dst)

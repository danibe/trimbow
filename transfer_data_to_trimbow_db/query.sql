with ph_data as (
	SELECT
	ph.code as pharmacy,
	to_char(s.prescrdate, 'YYYY-MM') as year_month,
	sum(case when length(pt.hash) = 64 then 1 else 0 end)::float / count(*)::float as ratio
	FROM
	public.sales s
	JOIN public.patient pt USING(patientdimid)
	JOIN public.pharmacy ph USING(pharmacydimid)
	JOIN public.product pr USING(productdimid)
	JOIN 
	(
		SELECT DISTINCT
		pha.pharmacy as pharmacy,
		aud.year_month
		FROM
		delivery_final.p_pha_panel pha
		JOIN delivery_final.panel_audit_v2 aud USING (panel_seq)
		WHERE aud.frequency = 'monthly' AND aud.audit = 'sales'
	) panel_phs ON (ph.code = panel_phs.pharmacy and to_char(s.prescrdate, 'YYYY-MM') = panel_phs.year_month)
	--WHERE s.prescrdate between '2018-11-01' and '2019-12-31' and pr.tos = 'P' and s.patientcontribution <> 100
	WHERE s.prescrdate between '{0}' and '{1}' and pr.tos = 'P' and s.patientcontribution <> 100
	GROUP BY ph.code, to_char(s.prescrdate, 'YYYY-MM')
),
continuos_phs as (
	SELECT DISTINCT
	pharmacy
	FROM ph_data
	GROUP BY pharmacy
	--HAVING sum(case when ratio > 0.60 then 1 else 0 end) >= 14
	HAVING sum(case when ratio > 0.60 then 1 else 0 end) >= {2}
),
trimbow_pts as (
	SELECT DISTINCT
	pt.hash as patient
	FROM
	public.sales s 
	JOIN public.patient pt USING(patientdimid)
	JOIN public.pharmacy ph USING(pharmacydimid)
	JOIN public.product p USING(productdimid)
	JOIN continuos_phs phs ON (ph.code = phs.pharmacy)
	--WHERE p.barcode in (SELECT barcode FROM danieleb.trimbow_druglist_support) and p.tos = 'P' and s.patientcontribution <> 100 and length(pt.hash) = 64 and s.prescrdate between '2018-11-01' and '2019-12-31'
	WHERE p.barcode in (SELECT barcode FROM danieleb.trimbow_druglist_support) and p.tos = 'P' and s.patientcontribution <> 100 and length(pt.hash) = 64 and s.prescrdate between '{0}' and '{1}'
)

SELECT
d.hash as doctor,
pt.hash as patient,
pt.yearofbirth as patient_yearofbirth,
null as patient_age,
pt.gender as patient_gender,
concat(pr.hash, '-', ph.code) as prescription,
pr.sales_trx_id as trx_id,
s.prescrdate::date as prescrdate,
p.barcode as barcode,
ae.atc_5_lvl as atc5,
ae.atc_5_lvl_desc as atc5_descr,
ae.atc_4_lvl as atc4,
ae.atc_4_lvl_desc as atc4_descr,
ae.atc_3_lvl as atc3,
ae.atc_3_lvl_desc as atc3_descr,
ae.atc_2_lvl as atc2,
ae.atc_2_lvl_desc as atc2_descr,
ae.atc_1_lvl as atc1,
ae.atc_1_lvl_desc as atc1_descr,
edl.pieces_per_package as pieces_per_package,
edl.commercial_name_latin as pack,
edl.commercial_name_only_latin as product,
s.price as price,
s.quantity as quantity,
ph.code as pharmacy,
geo.municipality_cil as municipality_cil,
geo.area_cil_8 as area_cil_8,
geo.subarea_cil_20 as subarea_cil_20,
geo.territory_cil_70 as territory_cil_70,
s.filedimid,
s.lineno,
i.icd10 as icd10,
null as isfirst
FROM public.sales s
JOIN public.patient pt USING(patientdimid)
JOIN trimbow_pts epts ON (pt.hash = epts.patient)
JOIN public.doctor d USING(doctordimid)
JOIN public.prescription pr ON (s.prescriptiondimid = pr.prescrdimid)
JOIN public.pharmacy ph USING(pharmacydimid)
JOIN public.product p USING(productdimid)
JOIN public.icd10 i USING(icd10dimid)
JOIN load.etesta_druglist edl USING(barcode)
JOIN load.atc_english ae ON (edl.atc = ae.atc_source)
JOIN crm.all_pharmacies crm ON (ph.code = crm.username)
JOIN geography.municipality_zip_mapping m ON (crm.zip = m.zipcode)
JOIN geography.geography_cil geo ON (m.municipality_cil = geo.municipality_cil)
--WHERE p.tos = 'P' and s.patientcontribution <> 100 and length(pt.hash) = 64 and s.prescrdate between '2019-09-01' and '2019-12-31';
WHERE p.tos = 'P' and s.patientcontribution <> 100 and length(pt.hash) = 64 and s.prescrdate between '{3}' and '{4}';
